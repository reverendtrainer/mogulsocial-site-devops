terraform {
  backend "s3" {
    bucket         = "mogulsocial-site-devops-tfstate"
    key            = "mogul-site.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "mogulsocial-site-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-west-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
