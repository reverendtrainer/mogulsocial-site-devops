variable "prefix" {
  default = "mogulsocialsite"
}

variable "project" {
  default = "mogulsocial-site-devops"
}

variable "contact" {
  default = "matt@thetrainermethod.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "mogulsocial-site-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "990005782159.dkr.ecr.us-west-2.amazonaws.com/mogulsocial-site-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for proxy"
  default     = "990005782159.dkr.ecr.us-west-2.amazonaws.com/mogulsocial-site-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

